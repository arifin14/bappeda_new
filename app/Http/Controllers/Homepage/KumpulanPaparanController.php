<?php

namespace App\Http\Controllers\Homepage;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\DataPaparan;
use App\Models\Visitors;

class KumpulanPaparanController extends Controller
{
    public function index(){
	$visitors = new Visitors();
    	$data['visitors']['yearly'] = $visitors->whereYear('created_at', '=', date('Y'))->count();
    	$data['visitors']['monthly'] = $visitors->whereMonth('created_at', '=', date('m'))->count();
        $data['title'] = 'Kumpulan Paparan';
        $data['paparan'] = DataPaparan::orderBy('id','desc');
        return view('content.kumpulanpaparan',$data);
    }

    public function slug_link($slug){
        $visitors = new Visitors();
            $data['visitors']['yearly'] = $visitors->whereYear('created_at', '=', date('Y'))->count();
            $data['visitors']['monthly'] = $visitors->whereMonth('created_at', '=', date('m'))->count();
            $data['title'] = 'Kumpulan Paparan';
            $data['paparan'] = DataPaparan::orderBy('id','desc');
            $data['slugnya'] = $slug;
            return view('content.kumpulanpaparanslug',$data);
        }
}
