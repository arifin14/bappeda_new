<?php

namespace App\Http\Controllers\Homepage;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Agenda;
use App\Models\Visitors;

class EventController extends Controller
{
    public function index(){
        $data['title'] = "Agenda";
	    $visitors = new Visitors();
    	$data['visitors']['yearly'] = $visitors->whereYear('created_at', '=', date('Y'))->count();
    	$data['visitors']['monthly'] = $visitors->whereMonth('created_at', '=', date('m'))->count();
        $agenda = Agenda::orderBy('id','desc')->paginate(5);
        $data['agenda'] = $agenda;
        if($agenda){
            // $data['agenda_terkini'] = Agenda::orderBy('schedule','desc')->take(5)->get();
            $tanggal_now = date('Y-m-d');
            $data['agendahariini'] = DB::select("SELECT * FROM agendas WHERE DATE(schedule) = '$tanggal_now' ORDER BY schedule,caption ASC");
            return view('content.event',$data);
        }
        else{
            return view('error-404');
        }
    }

    public function searchEvent(Request $request){
        $this->validate($request, [
            'keyword' => 'required|regex:/^[a-zA-Z]+$/u|max:150',
        ]);
        $data['title'] = 'Pencarian Agenda';
	    $visitors = new Visitors();
    	$data['visitors']['yearly'] = $visitors->whereYear('created_at', '=', date('Y'))->count();
    	$data['visitors']['monthly'] = $visitors->whereMonth('created_at', '=', date('m'))->count();
        if ($request->has('keyword')) {
            $agendas = Agenda::orderBy('id','desc')->where("caption",'like','%'.$request->keyword.'%')->get();
            $data['agendas'] = $agendas;
            // $data['agenda_terkini'] = Agenda::orderBy('schedule','desc')->take(5)->get();
            $tanggal_now = date('Y-m-d');
            $data['agendahariini'] = DB::select("SELECT * FROM agendas WHERE DATE(schedule) = '$tanggal_now' ORDER BY schedule,caption ASC");
            if($agendas){
                return view('content.search_event',$data);
            }
            else{
                return view('error-404'); 
            }
        }
        else{
            return view('error-404'); 
        }
        
        
    }

    public function clickEvent(Request $request){
        if($request->like)
        {
            DB::table('agendas')->increment('views', 1);
            return true;
        }
        else{
            return false;
        }
    }
}
